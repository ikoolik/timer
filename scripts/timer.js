const Helpers = (function () {
  function hasClass(el, className) {
    return el.classList
      ? el.classList.contains(className)
      : new RegExp("\\b" + className + "\\b").test(el.className);
  }

  function addClass(el, className) {
    if (el.classList) el.classList.add(className);
    else if (!KHelpers.hasClass(el, className)) el.className += " " + className;
  }

  function removeClass(el, className) {
    if (el.classList) el.classList.remove(className);
    else
      el.className = el.className.replace(
        new RegExp("\\b" + className + "\\b", "g"),
        ""
      );
  }

  return {
    hasClass, addClass, removeClass
  };
})();

class Timer {

  static init() {
    const body = document.getElementsByTagName("body")[0];
    const clock = document.createElement("div");

    Helpers.addClass(clock, "timer-clock");
    Helpers.addClass(clock, "timer-hidden");

    body.appendChild(clock);

    Timer.RUNNING = false;
    Timer.SOCKET = "module.timer";
    Timer.TIME_CURRENT = 0;
    Timer.TURN_DURATION = 19;
    Timer.CLOCK = clock;
    Timer.TICKERS = []

    game.socket.on(Timer.SOCKET, data => {
      if (!game.user.isGM) {
        if (data.timetick) {
          Timer.TIME_CURRENT = data.timetick;
          Timer.render()
        }
      }
    });

    Timer.toggleCheck();
  }

  static reset() {
    if (!game.user.isGM) return
    Timer.stop();
    Timer.start();
  }

  static start() {
    console.log('start');
    if (Helpers.hasClass(Timer.CLOCK, 'timer-hidden')) {
      Helpers.removeClass(Timer.CLOCK, 'timer-hidden');
    }

    if (!game.user.isGM) return

    Timer.RUNNING = true;
    Timer.tick();
    Timer.TICKERS.push(window.setInterval(Timer.tick, 1000))
  }

  static stop() {
    if (!Helpers.hasClass(Timer.CLOCK, 'timer-hidden')) {
      Helpers.addClass(Timer.CLOCK, 'timer-hidden');
    }
    if (!game.user.isGM) return

    Timer.RUNNING = false;
    for (let idx = Timer.TICKERS.length - 1; idx >= 0; --idx) {
      window.clearInterval(Timer.TICKERS[idx])
    }
    Timer.TIME_CURRENT = 0;
  }

  static pause() {
    console.log('pause');
    if (!game.user.isGM) return

    for (let idx = Timer.TICKERS.length - 1; idx >= 0; --idx) {
      window.clearInterval(Timer.TICKERS[idx])
    }
  }

  static resume() {
    console.log('resume');
    if (!game.user.isGM) return

    Timer.TICKERS.push(window.setInterval(Timer.tick, 1000))
  }

  static tick() {
    console.log('tick');
    if (!game.user.isGM) return

    Timer.TIME_CURRENT++;
    game.socket.emit(
      Timer.SOCKET,
      {
        senderId: game.user._id,
        type: "Number",
        timetick: Timer.TIME_CURRENT,
      },
      resp => {}
    );

    Timer.render()
    if(Timer.TIME_CURRENT >= Timer.TURN_DURATION) {
      Timer.pause()
    }
  }

  static render() {
    console.log('render');

    const remainer = Timer.TURN_DURATION - Timer.TIME_CURRENT;

    const combat = game.combats.active;
    const combatant = combat ? combat.combatant.actor.data.name : 'no one';

    let html = `
      <h1>${combatant}</h1>
      <span class="timer-time">${remainer}</span>
    `
    Timer.CLOCK.innerHTML = html;
  }

  static toggleCheck() {
    console.log('toggleCheck');
    const combat = game.combats.active;
    if(!combat && Timer.RUNNING) Timer.stop();

    if(combat) {
      if(Timer.RUNNING) Timer.reset();
      else Timer.start();
    }
  }
}


Hooks.on("ready", function () {
  Timer.init();
});
Hooks.on("updateCombat", function (data, delta) {
  Timer.toggleCheck();
});
Hooks.on("deleteCombat", function () {
  Timer.stop();
});
Hooks.on("pauseGame", function () {
  if (game.paused) Timer.pause();
  else Timer.resume();
});